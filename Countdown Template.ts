export class CountdownTemplate implements CampaignTemplateComponent {
    @title("Countdown Title")
    countTitle:String = "Time until event";
    @title("Background Image")
    bgUrl:String = "https://i.pinimg.com/originals/b1/20/25/b1202568d88e8fe9e02e7a67fa42c93e.jpg";
    @title("Countdown Target Date")
    targetDate:DateTime;
    @header('SET COLORS')
    titleColor:Color
    fontColor:Color
    boxColor:Color

    run(context: CampaignComponentContext) {
        return {};
    }

}
